package com.example.worktrack.Objects;

import android.text.style.StrikethroughSpan;

public class ToDoObject {

    private String taskName;
    private String taskTime;
    private String taskDate;

    public ToDoObject(String taskName, String taskTime, String taskDate) {
        this.taskName = taskName;
        this.taskTime = taskTime;
        this.taskDate = taskDate;

    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskTime() {
        return taskTime;
    }

    public String getTaskDate() {
        return taskDate;
    }

}
