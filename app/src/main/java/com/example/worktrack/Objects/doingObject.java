package com.example.worktrack.Objects;

public class doingObject {

    private String taskName;
    private String taskTime;
    private String taskDate;
    private int priority;

    public doingObject(String taskName, String taskTime, String taskDate, int priority) {
        this.taskName = taskName;
        this.taskTime = taskTime;
        this.taskDate = taskDate;
        this.priority = priority;

    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskTime() {
        return taskTime;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public int getPriority() {
        return priority;
    }
}
