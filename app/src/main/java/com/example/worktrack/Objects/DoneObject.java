package com.example.worktrack.Objects;

public class DoneObject {

    private String taskName;
    private String taskTime;
    private String taskDate;
    private String priority;
    private String label;

    public DoneObject(String taskName, String taskTime, String taskDate, String priority, String label) {
        this.taskName = taskName;
        this.taskTime = taskTime;
        this.taskDate = taskDate;
        this.priority = priority;
        this.label = label;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskTime() {
        return taskTime;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public String getPriority() {
        return priority;
    }

    public String getLabel() {
        return label;
    }

}
