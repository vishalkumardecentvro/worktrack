 package com.example.worktrack.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.worktrack.Doing;
import com.example.worktrack.Objects.ToDoObject;
import com.example.worktrack.R;
import com.example.worktrack.Objects.doingObject;
import com.example.worktrack.Room.TaskEntity;
import com.example.worktrack.ToDo;

import java.util.ArrayList;
import java.util.List;

public class DoingAdapter extends ArrayAdapter<doingObject> implements itemClick {

    private Context context;
    private int mResources;
    private int lasPosition = -1;
    private List<TaskEntity> doingList = new ArrayList<>();
    private Dialog changeTaskStateDialog;
    private Button taskResponse;


    static class ViewHolder {
        TextView taskName;
        TextView taskDate;
        TextView taskTime;
        TextView taskPriority;
        Spinner taskZoneSpinner;


    }

    public DoingAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.mResources = resource;
        this.context = context;
    }

    public void setTask(List<TaskEntity> tasks){
        this.doingList = tasks;

    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

//        String tskName = doingList.get(position).getTaskName();
//        String tskTime = doingList.get(position).getTaskTime();
//        String tskDate = doingList.get(position).getTaskDate();
//        int tskPriority = doingList.get(position).getPriority();

        TaskEntity currentTask = doingList.get(position);
        final View result;
        ViewHolder holder;

        if(convertView == null ){

            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(mResources,parent,false);

            holder = new ViewHolder();
            holder.taskName = convertView.findViewById(R.id.TaskNameText);
            holder.taskDate = convertView.findViewById(R.id.DateText);
            holder.taskTime = convertView.findViewById(R.id.TimeText);
            holder.taskZoneSpinner = convertView.findViewById(R.id.LabelSpinner);
            holder.taskPriority = convertView.findViewById(R.id.PriorityText);

            ArrayAdapter<CharSequence> zoneAdapter = ArrayAdapter.createFromResource(getContext(),R.array.TaskZone, android.R.layout.simple_spinner_item);
            zoneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.taskZoneSpinner.setAdapter(zoneAdapter);
            holder.taskZoneSpinner.setEnabled(false);

            Doing.taskList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Log.i("task posn", doingList.get(i).getTaskName());
                    Log.i("task posn", "inside taskList");
                    Log.i("taskList posn", String.valueOf(i));
                    int remove_posn = i;

                    Spinner spin = view.findViewById(R.id.LabelSpinner);
                    spin.setEnabled(true);

                    spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            Log.i("task","inside");
                            Log.i("task posn", String.valueOf(i));
                            String selectedText = adapterView.getItemAtPosition(i).toString();

                            if(selectedText.equals("To do")){
                                //Toast.makeText(getContext(),"inside spinner", Toast.LENGTH_SHORT).show();
                                changeTaskStateDialog = new Dialog(getContext());
                                changeTaskStateDialog.setContentView(R.layout.task_zone_dialog);
                                taskResponse = changeTaskStateDialog.findViewById(R.id.changeTaskZoneButton);
                                taskResponse.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        String name = doingList.get(remove_posn).getTaskName();
                                        String time = doingList.get(remove_posn).getTaskName();
                                        String date = doingList.get(remove_posn).getTaskName();

                                        ToDo.toDoData.add(new ToDoObject(name,time,date));
                                        ToDo.toDoAdapter.notifyDataSetChanged();

                                        Toast.makeText(getContext(), doingList.get(remove_posn).getTaskName()+" is remove from doing task section", Toast.LENGTH_LONG).show();
                                        Doing.doingList.remove(remove_posn);
                                        DoingAdapter.this.notifyDataSetChanged();
                                        changeTaskStateDialog.dismiss();
                                        spin.setEnabled(false);

                                    }
                                });

                                changeTaskStateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                changeTaskStateDialog.show();

                            }


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }

                    });


                }
            });


            result = convertView;
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder)  convertView.getTag();
            result = convertView;
        }


        holder.taskName.setText(currentTask.getTaskName());
        holder.taskDate.setText(currentTask.getDate());
        holder.taskTime.setText(currentTask.getTime());
        holder.taskPriority.setText(currentTask.getTaskPriority());

        Animation animation = new AnimationUtils().loadAnimation(context,(position>lasPosition)?R.anim.load_down_anim:R.anim.load_up_anim);
        result.startAnimation(animation);
        lasPosition = position;


        return convertView;

    }



}
