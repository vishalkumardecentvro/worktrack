package com.example.worktrack.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.worktrack.Objects.DoneObject;
import com.example.worktrack.R;

import java.util.List;

public class DoneAdapter extends ArrayAdapter<DoneObject> {
    int lastPosition = -1;
    int mResource;
    private Context context;

    class ViewHolder{
        TextView taskName;
        TextView taskTime;
        TextView taskDate;
        TextView taskPriority;
        TextView taskLabel;
    }


    public DoneAdapter(@NonNull Context context, int resource, @NonNull List<DoneObject> objects) {
        super(context, resource, objects);
        this.context = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String name = getItem(position).getTaskName();
        String time = getItem(position).getTaskTime();
        String date = getItem(position).getTaskDate();
        String priority = getItem(position).getPriority();
        String label = getItem(position).getLabel();

        ViewHolder holder;
        final View result;

        if(convertView == null){

            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(mResource,null,false);

            holder = new ViewHolder();

            holder.taskName = convertView.findViewById(R.id.TaskNameText);
            holder.taskLabel = convertView.findViewById(R.id.LabelText);
            holder.taskPriority = convertView.findViewById(R.id.PriorityText);
            holder.taskTime = convertView.findViewById(R.id.TimeText);
            holder.taskDate = convertView.findViewById(R.id.DateText);

            holder.taskName.setText(name);
            holder.taskDate.setText(date);
            holder.taskTime.setText(time);
            holder.taskLabel.setText(label);
            holder.taskPriority.setText(priority);


            result = convertView;
            convertView.setTag(holder);

        }else
        {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }


        Animation animation = new AnimationUtils().loadAnimation(context,(lastPosition>position)?R.anim.load_down_anim:R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        return convertView;
    }
}
