package com.example.worktrack.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.worktrack.R;
import com.example.worktrack.Objects.ToDoObject;

import java.util.List;



public class ToDoAdapter extends ArrayAdapter<ToDoObject> implements AdapterView.OnItemSelectedListener {

    int mResource;
    Context context;
    int lastPosition = -1;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    static class ViewHolder
    {
        TextView taskName;
        TextView taskTime;
        TextView taskDate;
    }

    public ToDoAdapter(@NonNull Context context, int resource, @NonNull List<ToDoObject> objects) {
        super(context, resource, objects);
        this.context = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String name = getItem(position).getTaskName();
        String time = getItem(position).getTaskTime();
        String date = getItem(position).getTaskDate();


        ViewHolder holder;
        final View result;

        if(convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(mResource,parent,false);

            holder = new ViewHolder();
            holder.taskName = convertView.findViewById(R.id.TaskNameText);
            holder.taskDate = convertView.findViewById(R.id.DateText);
            holder.taskTime = convertView.findViewById(R.id.TimeText);


            Spinner todoTaskZone = convertView.findViewById(R.id.todoSpinner);
            ArrayAdapter<CharSequence> todo = ArrayAdapter.createFromResource(getContext(),R.array.ToDoTaskZone, android.R.layout.simple_spinner_item);
            todo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            todoTaskZone.setAdapter(todo);
            todoTaskZone.setOnItemSelectedListener(this);

//            holder.taskName.setText(name);
//            holder.taskDate.setText(date);
//            holder.taskTime.setText(time);

            result = convertView;
            convertView.setTag(holder);


        } else
        {
            holder = (ViewHolder)  convertView.getTag();
            result = convertView;

        }

        holder.taskName.setText(name);
        holder.taskDate.setText(date);
        holder.taskTime.setText(time);
        //holder.taskPriority.setText(priority);
        //holder.taskLabel.setText(label);

        Animation animation = new AnimationUtils().loadAnimation(context,(lastPosition>position)?R.anim.load_down_anim:R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        return convertView;

    }
}
