package com.example.worktrack;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

public class AddTaskDialog extends AppCompatDialogFragment {

    private EditText taskName;
    private TextView taskTime;
    private TextView taskDate;
    private Button addButton;

    String zone;

    public AddTask listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_task_dialog,null);
        builder.setView(view).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).setPositiveButton("Add task", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String name = taskName.getText().toString();
                String date = taskDate.getText().toString();
                String time = taskTime.getText().toString();

                listener.applyTexts(name,date,time);


            }
        });

        taskName = view.findViewById(R.id.TaskNameEditText);
        taskDate = view.findViewById(R.id.TaskDateEditText);
        taskTime = view.findViewById(R.id.TaskTimeEditText);
        addButton = view.findViewById(R.id.AddTaskButton);

        taskDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                // get values of day,month and year, using Calender class
                Calendar c = Calendar.getInstance();
                int date = c.get(Calendar.DATE);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                taskDate.setText(date);

                            }
                        }, year, month, date);
                datePickerDialog.setTitle("Assign date");
                datePickerDialog.show();


            }
        });

        taskTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar time = Calendar.getInstance();
                int hour = time.get(Calendar.HOUR);
                int min = time.get(Calendar.MINUTE);

                TimePickerDialog timePick = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        if(i < 12) {
                            zone = "AM";
                        } else {
                            zone = "PM";
                        }
                        String time = i+":"+i1;//+" "+zone;
                        taskTime.setText(time);

                    }
                },hour,min,false);

                timePick.setTitle("Assign time");
                timePick.show();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try{
            listener = (AddTask) getTargetFragment();
        }catch (Exception e){
            throw new ClassCastException(context.toString()+"must implement dialog listener");
        }
    }

    public interface AddTask {

        void applyTexts(String name,String date,String time);

    }
}
