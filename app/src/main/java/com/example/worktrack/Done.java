package com.example.worktrack;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.worktrack.Adapters.DoneAdapter;
import com.example.worktrack.Objects.DoneObject;

import java.util.ArrayList;
import java.util.List;

public class Done extends Fragment {

    private ListView doneList;
    private List<DoneObject> doneObjectList = new ArrayList<>();
    private DoneAdapter doneAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View doneView = inflater.inflate(R.layout.done,null,false);
        return doneView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        doneObjectList.add(new DoneObject("Learn java","10:30","12-10-2020","1","doing"));
        doneObjectList.add(new DoneObject("Learn c","11:30","19-10-2020","2","doing"));
        doneObjectList.add(new DoneObject("Learn computer","12:30","15-10-2020","3","doing"));
        doneObjectList.add(new DoneObject("Learn gas","10:30","12-10-2020","4","doing"));
        doneObjectList.add(new DoneObject("Learn solid","10:30","12-10-2020","5","doing"));
        doneObjectList.add(new DoneObject("Learn math","10:30","12-10-2020","6","doing"));
        doneObjectList.add(new DoneObject("Learn english","10:30","12-10-2020","7","doing"));
        doneObjectList.add(new DoneObject("Learn fun","10:30","12-10-2020","8","doing"));

        doneList = view.findViewById(R.id.DoneList);
        doneAdapter = new DoneAdapter(getContext(),R.layout.custom_done,doneObjectList);
        doneList.setAdapter(doneAdapter);

    }
}
