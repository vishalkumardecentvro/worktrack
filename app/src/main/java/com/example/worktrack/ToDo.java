package com.example.worktrack;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.worktrack.Adapters.ToDoAdapter;
import com.example.worktrack.Objects.ToDoObject;
import com.example.worktrack.Objects.doingObject;
import com.example.worktrack.ui.main.PageViewModel;
import com.example.worktrack.ui.main.PlaceholderFragment;

import java.util.ArrayList;
import java.util.List;

public class ToDo extends Fragment implements AddTaskDialog.AddTask{

    ListView toDoList;
    public static List<ToDoObject> toDoData = new ArrayList<>();
    public static ToDoAdapter toDoAdapter;
    ImageView todoAdd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View toDoView = inflater.inflate(R.layout.to_do,container,false);
        return toDoView;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toDoData.add(new ToDoObject("Learn c","11:30","19-10-2020"));
        toDoData.add(new ToDoObject("Learn computer","12:30","15-10-2020"));
        toDoData.add(new ToDoObject("Learn gas","10:30","12-10-2020"));
        toDoData.add(new ToDoObject("Learn solid","10:30","12-10-2020"));
        toDoData.add(new ToDoObject("Learn math","10:30","12-10-2020"));
        toDoData.add(new ToDoObject("Learn english","10:30","12-10-2020"));
        toDoData.add(new ToDoObject("Learn fun","10:30","12-10-2020"));


        toDoList = view.findViewById(R.id.ToDoListView);
        todoAdd=view.findViewById(R.id.todoAddTask);

        todoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAddDialog();
            }
        });

        toDoAdapter = new ToDoAdapter(getContext(),R.layout.custom_to_do_view,toDoData);
        toDoList.setAdapter(toDoAdapter);

    }

    private void openAddDialog() {

        AddTaskDialog todoDialog = new AddTaskDialog();
        todoDialog.show(getFragmentManager(),"To Do add task");

    }

    @Override
    public void applyTexts(String name, String date, String time) {

        toDoData.add(new ToDoObject(name,time,date));
        toDoAdapter.notifyDataSetChanged();

    }
}
