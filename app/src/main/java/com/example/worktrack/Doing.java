package com.example.worktrack;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.example.worktrack.Adapters.DoingAdapter;
import com.example.worktrack.Objects.doingObject;
import com.example.worktrack.Room.TaskEntity;
import com.example.worktrack.Room.TaskViewModel;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

public class Doing extends Fragment {

    public  static ListView taskList;
    public  static DoingAdapter task;
    public static List<doingObject> doingList = new ArrayList<>();
    private ImageView addTask;
    private Dialog addTaskDialog;
    private Context context;
    String name,time,date;
    int prioirityLevel;
    private TaskViewModel taskViewModel;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        taskList = view.findViewById(R.id.DoingListView);
        addTask = view.findViewById(R.id.AddTask);
        context = getContext();



//        doingList.add(new doingObject("Learn java","10:30","12-10-2020", 1));
//        doingList.add(new doingObject("Learn c","11:30","19-10-2020", 1));
//        doingList.add(new doingObject("Learn computer","12:30","15-10-2020",1));
//        doingList.add(new doingObject("Learn gas","10:30","12-10-2020",1));
//        doingList.add(new doingObject("Learn solid","10:30","12-10-2020",1));
//        doingList.add(new doingObject("Learn math","10:30","12-10-2020",1));
//        doingList.add(new doingObject("Learn english","10:30","12-10-2020",1));
//        doingList.add(new doingObject("Learn fun","10:30","12-10-2020",1));
//
        taskViewModel.getAllTask().observe(this, new Observer<List<TaskEntity>>() {
            @Override
            public void onChanged(List<TaskEntity> taskEntities) {

            }
        });

        task = new DoingAdapter(getContext(),R.layout.custom_doing,doingList);
        taskList.setAdapter(task);

        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addTaskDialog = new Dialog(context);
                addTaskDialog.setContentView(R.layout.add_task_dialog);
                addTaskDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                addTaskDialog.show();
                verifiyInputs();
                Button Add = addTask.findViewById(R.id.AddTaskButton);
                Add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(verifiyInputs()){
//                            doingList.add(new doingObject(name,time,date,prioirityLevel));
//                            task.notifyDataSetChanged();

                            TaskEntity taskEntity = new TaskEntity(name,date,time,prioirityLevel);
                            taskViewModel.insert(taskEntity);
                            Toast.makeText(context, "Task saved", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
        });


    }

    private boolean verifiyInputs() {

        TextInputLayout taskName = addTaskDialog.findViewById(R.id.TaskTimeEditText);
        name = taskName.getEditText().toString();
        if(name.trim().isEmpty()){
            taskName.setError("You must enter task name");
            return false;
        }

        TextView taskDate = addTaskDialog.findViewById(R.id.TaskDateEditText);
        date = taskDate.toString();
        if(date.trim().isEmpty()){
            Toast.makeText(context, "Please select date", Toast.LENGTH_SHORT).show();
            return false;
        }

        TextView taskTime = addTaskDialog.findViewById(R.id.TaskTimeEditText);
        time = taskTime.toString();
        if(time.trim().isEmpty()){
            Toast.makeText(context, "Please select time", Toast.LENGTH_SHORT).show();
            return false;
        }

        TextInputLayout taskPriority = addTaskDialog.findViewById(R.id.TaskPriorityEditText);
        prioirityLevel = Integer.parseInt(taskPriority.getEditText().toString());
        if(String.valueOf(prioirityLevel).trim().isEmpty()){
            taskName.setError("You must enter priority for this task");
            return false;
        }
        if(prioirityLevel >10 || prioirityLevel <= 0){
            taskName.setError("Please provide appropriate level");
            return false;
        }

        return true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.doing, container, false);

        return root;
    }


}
