package com.example.worktrack.Room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

// In Dao we define all the operations which we want to make on our database
@Dao
public interface TaskDao {

    @Insert
    public void insert(TaskEntity task);

    @Delete
    public void delete(TaskEntity task);

    @Update
    public void update(TaskEntity task);

    @Query("SELECT * FROM task_table ORDER BY taskPriority ASC ")
    LiveData<List<TaskEntity>> getAllTask();

}
