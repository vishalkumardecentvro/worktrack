package com.example.worktrack.Room;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {TaskEntity.class},version = 1) // this will tell database which entity should be contained in database
public abstract class taskDatabase extends RoomDatabase {
    // taskDatabase connect entity and dao and create actual instance of database

    private static taskDatabase instance; // we have created this variable because we want this class to be singleton, so that no other instance can be created
    public abstract TaskDao taskDao();

    public static synchronized taskDatabase getInstance(Context context){ // synchronized means only one thread can at a time can access this method
        if(instance == null){

            instance = Room.databaseBuilder(context.getApplicationContext(),taskDatabase.class,"task_database")
                    .fallbackToDestructiveMigrationFrom()/* when we increment version number of this database we have to tell room how to migrate to
                    new schema, if we dont do this and try to increase number, our app will crash because we get illegal state exception, using this we
                    can avoid this because it will simply delete all the database and create it from scratch */
                    .build();
        }
        return instance;

    }

    public static RoomDatabase.Callback roomcallBack = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

        }
    };

    private static class populateDbAsyncTask extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }



}
