package com.example.worktrack.Room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "task_table")
public class TaskEntity {

    private String taskName;
    private String date;
    private String time;
    private int taskPriority;

    public TaskEntity(String taskName, String date, String time, int taskPriority) {
        this.taskName = taskName;
        this.date = date;
        this.time = time;
        this.taskPriority = taskPriority;

    }

    public int getTaskPriority() {
        return taskPriority;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    public String getTaskName() {
        return taskName;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

}
