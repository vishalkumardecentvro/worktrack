package com.example.worktrack.Room;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class TaskViewModel  extends AndroidViewModel {

    private taskRepository repository;
    private LiveData<List<TaskEntity>> allTask;

    public TaskViewModel(@NonNull Application application) {
        super(application);
        repository = new taskRepository(application);
        allTask = repository.getAllTask();
    }

    public void insert(TaskEntity taskEntity){
        repository.insert(taskEntity);
    }

    public void delete(TaskEntity taskEntity){
        repository.delete(taskEntity);
    }

    public void update(TaskEntity taskEntity){
        repository.update(taskEntity);
    }

    public LiveData<List<TaskEntity>> getAllTask(){
        return getAllTask();
    }
}
