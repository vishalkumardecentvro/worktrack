package com.example.worktrack.Room;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class taskRepository {

    private TaskDao taskDao;
    private LiveData<List<TaskEntity>> allTask;

    public taskRepository(Application application) {
        taskDatabase taskDb = taskDatabase.getInstance(application);
        this.taskDao = taskDb.taskDao();
        this.allTask = taskDao.getAllTask();
    }

    public void insert(TaskEntity taskEntity){

    }

    public void delete(TaskEntity taskEntity){

    }

    public void update(TaskEntity taskEntity){

    }

    public LiveData<List<TaskEntity>> getAllTask() {
        return allTask;
    }

    private static class InsertAsyncTask extends AsyncTask<TaskEntity,Void,Void>{

        private TaskDao taskDao;

        @Override
        protected Void doInBackground(TaskEntity... taskEntities) {
            taskDao.insert(taskEntities[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<TaskEntity,Void,Void>{
        private TaskDao taskDao;

        @Override
        protected Void doInBackground(TaskEntity... taskEntities) {
            taskDao.delete(taskEntities[0]);
            return null;
        }
    }

    private static class UpdateasyncTask extends AsyncTask<TaskEntity,Void,Void>{
        private TaskDao taskDao;

        @Override
        protected Void doInBackground(TaskEntity... taskEntities) {
            taskDao.update(taskEntities[0]);
            return null;
        }
    }
}
